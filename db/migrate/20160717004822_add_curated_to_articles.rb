class AddCuratedToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :curated, :boolean, default: false
  end
end
