class CreateFeaturedGroups < ActiveRecord::Migration
  def change
    create_table :featured_groups do |t|
      t.integer :featured_type
      t.string :title
      t.string :description
      t.string :cover_photo_url
      t.integer :opacity, default: 15
      t.integer :sequence
      t.boolean :hidden
      t.timestamps null: false
    end
  end
end
