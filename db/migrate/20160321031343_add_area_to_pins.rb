class AddAreaToPins < ActiveRecord::Migration
  def change
    add_column :pins, :area, :string
  end
end
