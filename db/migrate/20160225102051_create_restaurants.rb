class CreateRestaurants < ActiveRecord::Migration
  def change
    create_table :restaurants do |t|
      t.string :identifier
      t.string :name
      t.string :street
      t.string :city
      t.string :country
      t.decimal :latitude, :precision => 20, :scale => 15
      t.decimal :longitude, :precision => 20, :scale => 15
      t.string :category_id
      t.string :category_name
      t.timestamps null: false
    end
  end
end
