class AddExploreToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :explore, :boolean, default: true
  end
end
