class CreateSubcontents < ActiveRecord::Migration
  def change
    create_table :subcontents do |t|
      t.text :subtitle
      t.string :subphoto
      t.float :subphoto_width
      t.float :subphoto_height
      t.integer :sequence
      t.references :article
      t.timestamps null: false
    end
  end
end
