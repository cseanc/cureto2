class AddStarsCountToArticles < ActiveRecord::Migration
  def self.up
    add_column :articles, :stars_count, :integer, default: 0

    Article.reset_column_information
    Article.all.each do |article|
      Article.update_counters article.id, :stars_count => article.stars.length
    end
  end

  def self.down
    remove_column :articles, :stars_count
  end
end
