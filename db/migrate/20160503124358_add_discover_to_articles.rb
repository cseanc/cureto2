class AddDiscoverToArticles < ActiveRecord::Migration
  def change
    add_column :articles, :discover, :boolean, default: true
  end
end
