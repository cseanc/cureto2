class CreateFeaturedElements < ActiveRecord::Migration
  def change
    create_table :featured_elements do |t|
      t.integer :sequence
      t.integer :target_id, :limit => 8
      t.references :featured_group
      t.timestamps null: false
    end
  end
end
