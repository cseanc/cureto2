class CreateStars < ActiveRecord::Migration
  def change
    create_table :stars do |t|
      t.references :user
      t.references :article
      t.timestamps null: false
    end
  end
end
