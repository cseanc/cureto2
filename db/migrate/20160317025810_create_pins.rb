class CreatePins < ActiveRecord::Migration
  def change
    create_table :pins do |t|
      t.references :user
      t.references :article
      t.timestamps null: false
    end
  end
end
