class AddRestaurantIdToPins < ActiveRecord::Migration
  def change
    add_column :pins, :restaurant_id, :integer
  end
end
