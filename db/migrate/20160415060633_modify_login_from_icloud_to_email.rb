class ModifyLoginFromIcloudToEmail < ActiveRecord::Migration
  def change
    remove_column :users, :icloud_id
    add_column :users, :email_confirmed, :boolean, :default => false
    add_column :users, :confirm_token, :string
    add_column :users, :password_reset_token, :string
    add_column :users, :password_reset_sent_at, :datetime
  end
end
