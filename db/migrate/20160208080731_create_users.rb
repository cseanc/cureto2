class CreateUsers < ActiveRecord::Migration

  def change
    create_table :users do |t|
      t.string :icloud_id
      t.string :username
      t.string :email
      t.string :password_digest
      t.string :cover_photo
      t.string :display_name
      t.text :bio
      t.boolean :private
      t.boolean :active
      t.timestamps null: false
    end
  end
end
