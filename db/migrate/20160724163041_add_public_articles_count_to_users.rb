class AddPublicArticlesCountToUsers < ActiveRecord::Migration
  def change
    add_column :users, :public_articles_count, :integer, default: 0
  end
end
