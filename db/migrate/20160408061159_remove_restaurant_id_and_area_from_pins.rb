class RemoveRestaurantIdAndAreaFromPins < ActiveRecord::Migration
  def change
    remove_column :pins, :restaurant_id
    remove_column :pins, :area
  end
end
