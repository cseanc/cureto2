class CreateArticles < ActiveRecord::Migration
  def change
    create_table :articles do |t|
      t.string :title
      t.text :content
      t.string :cover_photo
      t.float :cover_photo_width
      t.float :cover_photo_height
      t.integer :food_type
      t.string :meal_type
      t.float :meal_price
      t.boolean :draft
      t.references :user
      t.references :restaurant
      t.timestamps null: false
    end
  end
end
