class CreateTokens < ActiveRecord::Migration
  def change
    create_table :tokens do |t|
      t.string :token_string
      t.datetime :expiry_date
      t.references :user
      t.timestamps null: false
    end
  end
end
