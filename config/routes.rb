Rails.application.routes.draw do
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  get '/' => 'void#emptiness'

  get 'users/confirm_email/:confirm_token' => 'users#confirm_email', as: 'confirm_email'
  get 'users/reset_password/:password_token' => 'users#reset_password', as: 'reset_password'
  patch 'users/send_password_reset' => 'users#send_password_reset'

  get 'password_reset/:password_token/edit' => 'password_reset#edit', as: 'password_reset'
  post 'password_reset' => 'password_reset#reset'

  post 'users/signup' => 'users#signup'
  post 'users/login' => 'users#login'

  get 'users/valid_username' => 'users#valid_username'
  get 'users/valid_email' => 'users#valid_email'
  post 'users/username' => 'users#username'

  get 'users/search' => 'users#search'

  get 'users/info' => 'users#info'
  get 'users/show/:id' => 'users#show'
  patch 'users/update' => 'users#update'
  patch 'users/update_password' => 'users#update_password'
  post 'users/change_cover_photo' => 'users#change_cover_photo'
  delete 'users/remove_cover_photo/:token' => 'users#remove_cover_photo'

  get 'users/update_public_articles_count' => 'users#update_public_articles_count'

  get 'users/discover' => 'users#discover'

  get 'tokens/validate/:token_string' => 'tokens#validate'
  delete 'tokens/:token_string' => 'tokens#destroy'
  delete 'tokens/:token_string/all' => 'tokens#destroy_all'

  get 'foursquare/public_authenticate' => 'foursquare#public_authenticate'
  get 'foursquare/authenticate' => 'foursquare#authenticate'

  get 'restaurants/areas' => 'restaurants#areas'
  get 'restaurants/areas_with_picture' => 'restaurants#areas_with_picture'
  post 'restaurants' => 'restaurants#retrieve_else_create'

  get 'articles' => 'articles#index'
  get 'articles/curated' => 'articles#curated'
  get 'articles/first' => 'articles#first'
  get 'articles/chill' => 'articles#chill'
  get 'articles/explore' => 'articles#explore'
  get 'articles/for_featured_group' => 'articles#for_featured_group'
  get 'articles/for_user' => 'articles#for_user'
  get 'articles/for_restaurant' => 'articles#for_restaurant'
  get 'articles/count_for_user' => 'articles#count_for_user'
  get 'articles/drafts_for_user' => 'articles#drafts_for_user'
  get 'articles/retrieve_for_user/:id' => 'articles#retrieve_for_user'
  post 'articles/destroy_for_user/:id' => 'articles#destroy_for_user'
  post 'articles' => 'articles#create'
  post 'articles/draft' => 'articles#draft'
  patch 'articles/update/:id' => 'articles#update'
  patch 'articles/change_cover_photo/:id' => 'articles#change_cover_photo'

  get 'articles/relationship_with_user/:article_id' => 'articles#relationship_with_user'
  
  post 'subcontents' => 'subcontents#create'
  patch 'subcontents/update/:id' => 'subcontents#update'
  patch 'subcontents/change_subphoto/:id' => 'subcontents#change_subphoto'
  post 'subcontents/destroy_for_user/:id' => 'subcontents#destroy_for_user'

  get 'pins/areas_with_picture' => 'pins#areas_with_picture'
  get 'pins/for_user_within_area' => 'pins#for_user_within_area'
  get 'pins/for_user' => 'pins#for_user'
  get 'pins/for_user_by_area' => 'pins#for_user_by_area'
  post 'pins' => 'pins#create'
  delete 'pins/destroy_for_user/:article_id' => 'pins#destroy_for_user'

  get 'stars/count_for_article/:article_id' => 'stars#count_for_article'
  post 'stars' => 'stars#create'
  delete 'stars/destroy_for_user/:article_id' => 'stars#destroy_for_user'

  get 'messages/for_user' => 'messages#for_user'
  get 'messages/unread_count_for_user' => 'messages#unread_count_for_user'
  patch 'messages/mark_all_read' => 'messages#mark_all_read'
  post 'messages' => 'messages#create'

  get 'featured_groups' => 'featured_groups#index'
  get 'featured_groups/last_updated' => 'featured_groups#last_updated'
  get 'featured_elements/:group_id' => 'featured_elements#show'

  # mark: - admin routes
  # get 'users' => 'users#index'
  # post 'users/admin_signup' => 'users#admin_signup'
  # delete 'users/:id' => 'users#destroy'

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  get '*path' => 'void#emptiness'

end
