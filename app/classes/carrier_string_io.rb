class CarrierStringIO < StringIO

  def original_filename
    "#{Time.now.strftime("%Y-%m-%d_%H%M")}.jpg"
  end

  def content_type
    "image/jpg"
  end

end