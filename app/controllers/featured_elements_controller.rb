class FeaturedElementsController < ApplicationController

  def show
    if params && params[:group_id]
      elements = FeaturedElement.where(:featured_group_id => params[:group_id]).order('sequence')
      render :json => elements, :status => 200
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

end
