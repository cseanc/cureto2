class PinsController < ApplicationController
  before_action :valid_token

  require 'kaminari'

  def areas_with_picture
    areas = Pin.areas_with_picture(valid_token.user_id)
    render :json => areas, :status => 200
  end

  def for_user_within_area
    articles = Pin.for_user_within_area(valid_token.user_id, params[:area])
    render :json => articles, :status => 200
  end

  def for_user
    pins = Pin.for_user(valid_token.user_id)
    articles ||= []
    pins.each do |pin|
      articles << pin.article
    end
    item_per_page = Rails.application.config.item_per_page
    paginated_articles = Kaminari.paginate_array(articles).page(params[:page]).per(item_per_page)
    render :json => paginated_articles, :status => 200
  end

  # def for_user_by_area
  #   pins = Pin.for_user_by_area(valid_token.user_id)
  #   grouped_pins = pins.group_by(&:area)
  #   articles ||= {}
  #   grouped_pins.each_pair do |area, pins|
  #     area_articles ||= []
  #     pins.each do |pin|
  #       area_articles << article_json(pin.article)
  #     end
  #     articles[area] = area_articles
  #   end
  #   render :json => articles, :status => 200
  # end

  def for_user_by_area
    # pins = Pin.for_user_by_area(valid_token.user_id)
    # render :json => pins, :status => 200
    grouped_pins = Pin.for_user_by_area(valid_token.user_id)
    articles ||= {}
    grouped_pins.each do |area, grouped_articles|
      area_articles ||= []
      grouped_articles.each do |article|
        area_articles << article_json(article)
      end
      articles[area] = area_articles
    end
    render :json => articles, :status => 200
  end

  def create
    pin = Pin.new(pin_params)
    pin.user_id = valid_token.user_id
    if pin.save
      render :json => {message: 'Pinned'}, :status => 201
    else
      render :json => {error: 'Unable to pin'}, :status => 400
    end
  end

  def destroy_for_user
    pin = Pin.where(:user_id => valid_token.user_id, :article_id => params[:article_id]).first
    if pin && pin.destroy
      render :json => {message: 'Pin destroyed'}, :status => 200
    else
      render :json => {error: 'Unable to destroy pin'}, :status => 400
    end
  end

  private
  def pin_params
    params.require(:pin).permit(:user_id, :article_id)
  end

  def article_json(article)
    json = {
        'id': article.id,
        'title': article.title,
        'content': article.content,
        'cover_photo': article.cover_photo,
        'cover_photo_width': article.cover_photo_width,
        'cover_photo_height': article.cover_photo_height,
        'food_type': article.food_type,
        'meal_type': article.meal_type,
        'meal_price': article.meal_price,
        'draft': article.draft
    }

    user = {
        'username': article.user.username,
        'display_name': article.user.display_name,
        'bio': article.user.bio,
        'cover_photo': article.user.cover_photo
    }
    json['user'] = user

    restaurant = {
        'id': article.restaurant.id,
        'identifier': article.restaurant.identifier,
        'name': article.restaurant.name,
        'street': article.restaurant.street,
        'city': article.restaurant.city,
        'country': article.restaurant.country,
        'latitude': article.restaurant.latitude,
        'longitude': article.restaurant.longitude,
        'category_id': article.restaurant.category_id,
        'category_name': article.restaurant.category_name
    }
    json['restaurant'] = restaurant

    subcontents ||= []
    article.subcontents.each do |s|
      subcontent = {
          'id': s.id,
          'subtitle': s.subtitle,
          'subphoto': s.subphoto,
          'subphoto_width': s.subphoto_width,
          'subphoto_height': s.subphoto_height,
          'article_id': s.article_id
      }
      subcontents << subcontent
    end
    json['subcontents'] = subcontents

    return json
  end

end
