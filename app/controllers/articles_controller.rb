class ArticlesController < ApplicationController
  before_action :valid_token, :only => [:drafts_for_user, :create, :draft, :update, :change_cover_photo, 
                                        :retrieve_for_user, :destroy_for_user, :relationship_with_user]
  
  require 'kaminari'
  
  def index
    item_per_page = Rails.application.config.item_per_page
    if params[:type] && params[:price]
      articles = Article.for_type_and_price(params[:type], params[:price]).page(params[:page]).per(item_per_page)
    elsif params[:type]
      articles = Article.for_type(params[:type]).page(params[:page]).per(item_per_page)
    elsif params[:price]
      articles = Article.for_price(params[:price]).page(params[:page]).per(item_per_page)
    else
      articles = Article.home.page(params[:page]).per(item_per_page)
    end
    render :json => articles, :status => 200
  end

  def curated
    item_per_page = Rails.application.config.item_per_page
    params[:seed] ||= Random.new_seed
    srand params[:seed].to_i
    articles = Kaminari.paginate_array(Article.not_draft_curated.shuffle).page(params[:page]).per(item_per_page)
    render :json => articles, :status => 200
  end

  def first
    if params[:type] && params[:price]
      article = Article.for_type_and_price(params[:type], params[:price]).first
    elsif  params[:type]
      article = Article.for_type(params[:type]).first
    elsif params[:price]
      article = Article.for_price(params[:price]).first
    else
      article = Article.home.first
    end
    render :json => article, :status => 200
  end

  def chill
    articles = Article.chill(params[:area], params[:type], params[:lower_price], params[:upper_price])
    render :json => articles, :status => 200
  end

  def explore
    articles = Article.explore(params[:area])
    render :json => articles, :status => 200
  end

  def for_featured_group
    ids = params['ids'].split(',').map(&:strip)
    articles ||= []
    ids.each do |id|
      begin
        article = Article.find(id)
        articles << article
      rescue

      end
    end
    render :json => articles, :status => 200
  end
  
  def for_user
    if params && params[:username]
      user = User.find_by_username(params[:username])
      if user
        item_per_page = Rails.application.config.item_per_page
        articles = Article.not_draft_desc_for_user(user.id).page(params[:page]).per(item_per_page)
        render :json => articles, :status => 200
      else
        render :json => {error: 'User not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end

  def for_restaurant
    if params && params[:id]
      item_per_page = Rails.application.config.item_per_page
      articles = Article.not_draft_desc_for_restaurant(params[:id]).page(params[:page]).per(item_per_page)
      render :json => articles, :status => 200
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def count_for_user
    if params && params[:username]
      user = User.find_by_username(params[:username])
      if user
        count = Article.not_draft_desc_for_user(user.id).count
        render :json => {count: count}, :status => 200
      else
        render :json => {error: 'User not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def drafts_for_user
    item_per_page = Rails.application.config.item_per_page
    articles = Article.draft_desc_for_user(valid_token.user_id).page(params[:page]).per(item_per_page)
    render :json => articles, :status => 200
  end
  
  def retrieve_for_user
    article = Article.find(params[:id])
    if article
      if article.user_id == valid_token.user_id
        render :json => article, :status => 200
      else
        render :json => {error: 'Not allowed'}, :status => 403
      end
    else
      render :json => {error: 'Article not found'}, :status => 400
    end
  end
    
  def create
    if params && params[:article][:cover_photo]
      article = Article.new(article_params)
      article.user_id = valid_token.user_id
      article.draft = false
      article.image_data = params[:article][:cover_photo]
      if article.save
        update_user_public_articles_count(valid_token.user_id)
        render :json => article, :status => 201
      else
        render :json => {error: 'Unable to create article'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def draft
    if params && params[:article][:cover_photo]
      article = Article.new(article_params)
      article.user_id = valid_token.user_id
      article.draft = true
      article.image_data = params[:article][:cover_photo]
      if article.save
        render :json => article, :status => 201
      else
        render :json => {error: 'Unable to create article'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def update
    article = Article.find(params[:id])
    if article
      if article.user_id == valid_token.user_id
        originally_draft = article.draft
        if article.update_attributes(article_update_params) && article.touch
          currently_draft = article.draft
          if originally_draft && !currently_draft
            article.created_at = Time.now
            if article.save
              update_user_public_articles_count(valid_token.user_id)
            end
          end
          render :json => {message: 'Article updated'}, :status => 200
        else
          render :json => {error: 'Unable to update article'}, :status => 400
        end
      else
        render :json => {error: 'Not allowed'}, :status => 403
      end
    else
      render :json => {error: 'Article not found'}, :status => 400
    end
  end
  
  def change_cover_photo
    if params && params[:id] && params[:cover_photo] && params[:cover_photo_width] && params[:cover_photo_height]
      article = Article.find(params[:id])
      if article
        if article.user_id == valid_token.user_id
          article.cover_photo_width = params[:cover_photo_width]
          article.cover_photo_height = params[:cover_photo_height]
          article.image_data = params[:cover_photo]
          if article.save
            render :json => {message: 'Article updated'}, :status => 200
          else
            render :json => {error: 'Unable to update article'}, :status => 400
          end
        else
          render :json => {error: 'Not allowed'}, :status => 403
        end
      else
        render :json => {error: 'Article not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def destroy_for_user
    article = Article.find(params[:id])
    if article
      if article.user_id == valid_token.user_id
        if article.destroy
          update_user_public_articles_count(valid_token.user_id)
          render :json => {message: 'Article destroyed'}, :status => 200
        else
          render :json => {error: 'Unable to destroy article'}, :status => 400
        end
      else
        render :json => {error: 'Not allowed'}, :status => 403
      end
    else
      render :json => {error: 'Article not found'}, :status => 400
    end
  end

  def relationship_with_user
    article = Article.find(params[:article_id])
    if article
      starred = false
      pinned = false

      star = Star.where(:user_id => valid_token.user_id, :article_id => article.id).first
      if star
        starred = true
      end

      pin = Pin.where(:user_id => valid_token.user_id, :article_id => article.id).first
      if pin
        pinned = true
      end

      render :json => {starred: starred, pinned: pinned}, :status => 200

    else
      render :json => {error: 'Article not found'}, :status => 400
    end
  end

  # def for_pins_by_area
  #   articles = Article.for_pins_by_area(valid_token.user_id)
  #   render :json => articles, :status => 200
  # end
  
  private
  def article_params
    params.require(:article).permit(:title, :content, :cover_photo_width, :cover_photo_height, :food_type, :meal_type, :meal_price, :restaurant_id)
  end
  
  def article_update_params
    params.require(:article).permit(:title, :content, :draft, :food_type, :meal_type, :meal_price, :restaurant_id)
  end

  def update_user_public_articles_count(user_id)
    user = User.find(user_id)
    user.public_articles_count = Article.where(user_id: user.id, draft: false).count
    user.save
  end
  
end
