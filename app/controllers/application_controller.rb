class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include ActionController::HttpAuthentication::Token::ControllerMethods
  include ::ActionController::Serialization
  helper_method :valid_token

  private
  def valid_token
    authenticate_or_request_with_http_token do |token, options|
      token = Token.find_by_token_string(token)
    end
  end

end
