class PasswordResetController < ApplicationController

  def edit
    user = User.find_by_password_reset_token(params[:password_token])
    if user && user.password_reset_sent_at > 1.hour.ago
      @token = user.password_reset_token
      render 'password_reset/edit'
    else
      redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/password_token_expired.html'
    end
  end

  def reset
    user = User.find_by_password_reset_token(params[:password_token])
    if user && user.password_reset_sent_at > 1.hour.ago
      user.password = params[:password]
      user.password_reset_token = nil
      user.password_reset_sent_at = nil
      if user.save
        redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/password_reset_successful.html'
      else
        redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/password_token_expired.html'
      end
    else
      redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/password_token_expired.html'
    end
  end

end
