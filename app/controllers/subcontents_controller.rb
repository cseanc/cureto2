class SubcontentsController < ApplicationController
  before_action :valid_token
  
  def create
    if params && params[:subcontent][:subphoto]
      article = Article.find(params[:subcontent][:article_id])
      if article && article.user_id == valid_token.user_id
        subcontent = Subcontent.new(subcontent_params)
        subcontent.image_data = params[:subcontent][:subphoto]
        if subcontent.save
          render :json => {message: 'Subcontent created'}, :status => 201
        else
          render :json => {error: 'Unable to create subcontent'}, :status => 400
        end
      else
        render :json => {error: 'Article not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def update
    subcontent = Subcontent.find(params[:id])
    if subcontent
      article = Article.find(subcontent.article_id)
      if article
        if article.user_id == valid_token.user_id
          if subcontent.update_attributes(subcontent_update_params)
            render :json => {message: 'Subcontent updated'}, :status => 200
          else
            render :json => {error: 'Unable to update subcontent'}, :status => 400
          end
        else
          render :json => {error: 'Not allowed'}, :status => 403
        end
      else
        render :json => {error: 'Article not found'}, :status => 400
      end
    else
      render :json => {error: 'Subcontent not found'}, :status => 400
    end
  end
  
  def change_subphoto
    if params && params[:id] && params[:subphoto] && params[:subphoto_width] && params[:subphoto_height]
      subcontent = Subcontent.find(params[:id])
      if subcontent
        article = Article.find(subcontent.article_id)
        if article
          if article.user_id == valid_token.user_id
            subcontent.subphoto_width = params[:subphoto_width]
            subcontent.subphoto_height = params[:subphoto_height]
            subcontent.image_data = params[:subphoto]
            if subcontent.save
              render :json => {message: 'Subcontent updated'}, :status => 200
            else
              render :json => {error: 'Unable to update subcontent'}, :status => 400
            end
          else
            render :json => {error: 'Not allowed'}, :status => 403
          end
        else
          render :json => {error: 'Article not found'}, :status => 400
        end
      else
        render :json => {error: 'Subcontent not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  def destroy_for_user
    subcontent = Subcontent.find(params[:id])
    if subcontent
      article = Article.find(subcontent.article_id)
      if article
        if article.user_id == valid_token.user_id
          if subcontent.destroy
            render :json => {message: 'Subcontent destroyed'}, :status => 200
          else
            render :json => {error: 'Unable to destroy subcontent'}, :status => 400
          end
        else
          render :json => {error: 'Not allowed'}, :status => 403
        end
      else
        render :json => {error: 'Article not found'}, :status => 400
      end
    else
      render :json => {error: 'Subcontent not found'}, :status => 400
    end
  end
  
  private
  def subcontent_params
    params.require(:subcontent).permit(:subtitle, :sequence, :subphoto_width, :subphoto_height, :article_id)
  end
  
  def subcontent_update_params
    params.require(:subcontent).permit(:subtitle, :sequence)
  end
  
end
