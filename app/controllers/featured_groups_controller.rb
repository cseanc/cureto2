class FeaturedGroupsController < ApplicationController

  def index
    groups = FeaturedGroup.where(:hidden => false).order('sequence')
    render :json => groups, :status => 200
  end

  def last_updated
    group = FeaturedGroup.all.first
    render :json => {updated: group.updated_at}, :status => 200
  end

end
