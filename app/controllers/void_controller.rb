class VoidController < ApplicationController

  def emptiness
    render :json => {message: 'Welcome to the infinite emptiness!'}, :status => 404
  end

end
