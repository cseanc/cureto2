class UsersController < ApplicationController
  http_basic_authenticate_with :name => ENV['API_AUTH_NAME'],
                               :password => ENV['API_AUTH_PASSWORD'],
                               :only => [:signup, :login, :valid_username, :valid_email,
                                         :username, :remove_cover_photo, :send_password_reset,
                                         :update_public_articles_count]
  before_action :valid_token, :only => [:update, :update_password, :change_cover_photo]
  require 'securerandom'

  def update_public_articles_count
    User.all.each do |user|
      user.public_articles_count = Article.where(user_id: user.id, draft: false).count
      user.save
    end
    render :json => {message: 'updated'}, :status => 200
  end

  def discover
    item_per_page = Rails.application.config.item_per_page
    params[:seed] ||= Random.new_seed
    srand params[:seed].to_i
    users = Kaminari.paginate_array(User.discover.shuffle).page(params[:page]).per(item_per_page)

    render :json => users, :status => 200
  end

  def signup
    password = AESCrypt.decrypt(params[:user][:password], ENV['API_AES_SECRET'])
    user = User.new(:username => params[:user][:username], :email => params[:user][:email],
                    :password => password)
    user.display_name = params[:user][:username]
    user.user_type = 1
    user.private = false
    user.active = true
    if user.save
      UserMailer.registration_confirmation(user).deliver_now
      render :json => {message: 'User created'}, :status => 201
    else
      render :json => {error: user.errors.full_messages}, :status => 400
    end
  end

  def login
    password = AESCrypt.decrypt(params[:user][:password], ENV['API_AES_SECRET'])
    user = User.find_by_username(params[:user][:username])
    if user && user.authenticate(password)
      if user.active
        token = Token.new(:user_id => user.id)
        if token.save
          render :json => {token_string: token.token_string, username: user.username}, :status => 200
        else
          render :json => {error: 'Login failed'}, :status => 400
        end
      else
        render :json => {error: 'Your account is not yet activated'}, :status => 403
      end
    else
      render :json => {error: 'Wrong email or password'}, :status => 403
    end
  end

  def confirm_email
    user = User.find_by_confirm_token(params[:confirm_token])
    if user
      user.confirm_email
      redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/email_verified.html'
    else
      redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/email_not_verified.html'
    end
  end

  def send_password_reset
    user = User.find_by_email(params[:email])
    if user
      user.send_password_reset_token
      render :json => {message: 'Password reset instruction has been sent to your email'}, :status => 200
    else
      render :json => {error: 'This email is not associated with any account in Cureto'}, :status => 400
    end
  end

  def reset_password
    user = User.find_by_password_reset_token(params[:password_token])
    if user && user.password_reset_sent_at > 1.hour.ago
      redirect_to password_reset_url(params[:password_token])
    else
      redirect_to 'https://s3-ap-southeast-1.amazonaws.com/cureto/policies_and_terms/password_token_expired.html'
    end
  end

  def valid_username
    if params && params[:q]
      user = User.find_by_username(params[:q])
      if user
        render :json => {message: 'Duplicated'}, :status => 302
      else
        render :json => {message: 'Available'}, :status => 200
      end
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

  def valid_email
    if params && params[:q]
      user = User.find_by_email(params[:q])
      if user
        render :json => {message: 'Duplicated'}, :status => 302
      else
        render :json => {message: 'Available'}, :status => 200
      end
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

  def username
    if params && params[:q]
      icloud_id = AESCrypt.decrypt(params[:q], ENV['API_AES_SECRET'])
      user = User.find_by_icloud_id(icloud_id)
      if user
        render :json => {username: user.username}, :status => 200
      else
        render :json => {error: 'No such user'}, :status => 404
      end
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

  def show
    user = User.find(params[:id])
    if user
      render :json => user, :status => 200
    else
      render :json => {error: 'User not found'}, :status => 400
    end
  end

  def info
    if params && params[:username]
      user = User.find_by_username(params[:username])
      if user
        render :json => user, :status => 200
      else
        render :json => {error: 'User not found'}, :status => 400
      end
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

  def update
    user = User.find(valid_token.user_id)
    if user
      if user.update_attributes(user_update_params)
        render :json => {message: 'User updated'}, :status => 200
      else
        render :json => {error: 'Unable to update user'}, :status => 400
      end
    else
      render :json => {error: 'User not found'}, :status => 400
    end
  end

  def update_password
    user = User.find(valid_token.user_id)
    original_password = AESCrypt.decrypt(params[:original], ENV['API_AES_SECRET'])
    new_password = AESCrypt.decrypt(params[:new], ENV['API_AES_SECRET'])
    if user && user.authenticate(original_password)
      user.password = new_password
      if user.save
        render :json => {message: 'Password Changed'}, :status => 200
      else
        render :json => {error: 'Password change failed. Please try again later'}, :status => 400
      end
    else
      render :json => {error: 'Please enter the correct original password'}, :status => 403
    end
  end

  def change_cover_photo
    user = User.find(valid_token.user_id)
    if user
      if params && params[:cover_photo]
        user.image_data = params[:cover_photo]
        if user.save
          render :json => {message: 'Cover photo changed'}, :status => 200
        else
          render :json => {error: 'Unable to change cover photo'}, :status => 400
        end
      else
        render :json => {error: 'Missing required parameters'}, :status => 400
      end
    else
      render :json => {error: 'User not found'}, :status => 400
    end
  end

  def remove_cover_photo
    if params && params[:token]
      token = Token.find_by_token_string(params[:token])
      if token
        user = User.find(token.user_id)
        if user
          user.remove_cover_photo!
          if user.save
            render :json => {message: 'Cover photo removed'}, :status => 200
          else
            render :json => {error: 'Unable to remove cover photo'}, :status => 400
          end
        else
          render :json => {error: 'User not found'}, :status => 400
        end
      else
        render :json => {error: 'Invalid token'}, :status => 400
      end
    else
      render :json => {error: 'Missing required parameters'}, :status => 400
    end
  end

  def search
    curators = User.where('(username LIKE ? or display_name LIKE ?) and private = ? and user_type = ?', "%#{params[:q]}%", "%#{params[:q]}%", false, 1).limit(15)
    render :json => curators, :status => 200
  end

  private
  def user_params
    params.require(:user).permit(:username, :email, :password)
  end

  def user_update_params
    params.require(:user).permit(:display_name, :bio)
  end

end
