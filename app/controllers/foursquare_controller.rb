class FoursquareController < ApplicationController
  http_basic_authenticate_with :name => ENV['API_AUTH_NAME'],
                               :password => ENV['API_AUTH_PASSWORD'],
                               :only => [:public_authenticate]
  before_action :valid_token, :only => [:authenticate]

  require 'aescrypt'

  def public_authenticate
    render :json => {
        client_id: AESCrypt.encrypt(ENV['FOURSQUARE_CLIENT_ID'], ENV['API_AES_SECRET']),
        client_secret: AESCrypt.encrypt(ENV['FOURSQUARE_CLIENT_SECRET'], ENV['API_AES_SECRET'])
    }, :status => 200
  end
  
  def authenticate
    render :json => {
      client_id: AESCrypt.encrypt(ENV['FOURSQUARE_CLIENT_ID'], ENV['API_AES_SECRET']),
      client_secret: AESCrypt.encrypt(ENV['FOURSQUARE_CLIENT_SECRET'], ENV['API_AES_SECRET'])
      }, :status => 200
  end
  
end
