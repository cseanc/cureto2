class TokensController < ApplicationController
  http_basic_authenticate_with :name => ENV['API_AUTH_NAME'],
                               :password => ENV['API_AUTH_PASSWORD'],
                               :only => [:validate, :destroy, :destroy_all]

  def validate
    token = Token.find_by_token_string(params[:token_string])
    if token
      render :json => {message: 'Valid token'}, :status => 200
    else
      render :json => {error: 'Invalid token'}, :status => 400
    end
  end

  def destroy
    if params && params[:token_string]
      token = Token.find_by_token_string(params[:token_string])
      if token && token.destroy
        render :json => {message: 'Logged out'}, :status => 200
      else
        render :json => {error: 'Token not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end

  def destroy_all
    if params && params[:token_string]
      token = Token.find_by_token_string(params[:token_string])
      if token
        tokens = Token.where(:user_id => token.user_id)
        if tokens && tokens.destroy_all
          render :json => {message: 'Logged out from all devices'}, :status => 200
        else
          render :json => {error: 'Tokens not found'}, :status => 400
        end
      else
        render :json => {error: 'Token not found'}, :status => 400
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end

end
