class StarsController < ApplicationController
  before_action :valid_token, :except => [:count_for_article]

  def count_for_article
    count = Star.where(:article_id => params[:article_id]).count
    render :json => {message: count}, :status => 200
  end

  def create
    star = Star.new(:user_id => valid_token.user_id, :article_id => params[:article_id])
    if star.save
      render :json => {message: 'Star created'}, :status => 201
    else
      render :json => {error: 'Unable to create star'}, :status => 400
    end
  end

  def destroy_for_user
    star = Star.where(:user_id => valid_token.user_id, :article_id => params[:article_id]).first
    if star && star.destroy
      render :json => {message: 'Star destroyed'}, :status => 200
    else
      render :json => {error: 'Unable to destroy star'}, :status => 400
    end
  end

  private
  def star_params
    params.require(:star).permit(:user_id, :article_id)
  end

end
