class RestaurantsController < ApplicationController
  before_action :valid_token, :only => [:retrieve_else_create]

  def areas
    areas = Restaurant.where.not(:city => nil).order('city').uniq.pluck(:city)
    discovered_areas ||= []
    areas.each do |area|
      count = Article.joins(:restaurant).where('draft = ? and discover = ? and restaurants.city = ?',
                                               false, true, area).count
      if count > 0
        discovered_areas << area
      end
    end
    render :json => discovered_areas, :status => 200
  end

  def areas_with_picture
    areas = Restaurant.where.not(:city => nil).order('city').uniq.pluck(:city)
    discovered_areas ||= []

    areas.each do |area|
      articles = Article.joins(:restaurant).where('draft = ? and discover = ? and explore = ? and restaurants.city = ?', false, true, true, area).order('RAND()').limit(1)
      if articles.count > 0
        article = articles.first
        discovered_areas << {area: area, photo: article.cover_photo.url}
      end
    end

    render :json => discovered_areas, :status => 200
  end
  
  def retrieve_else_create
    if params && params[:restaurant][:identifier]
      existing_restaurant = Restaurant.find_by_identifier(params[:restaurant][:identifier])
      if existing_restaurant
        render :json => existing_restaurant, :status => 200
      else
        restaurant = Restaurant.new(restaurant_params)
        if restaurant.save
          render :json => restaurant, :status => 201
        else
          render :json => {error: 'Unable to create restaurant'}, :status => 400
        end
      end
    else
      render :json => {error: 'Required parameters missing'}, :status => 400
    end
  end
  
  private
  def restaurant_params
    params.require(:restaurant).permit(:identifier, :name, :street, :city, :country, :latitude, :longitude, :category_id, :category_name)
  end
  
end
