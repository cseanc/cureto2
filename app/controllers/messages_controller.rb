class MessagesController < ApplicationController
  before_action :valid_token

  require 'kaminari'

  def for_user
    messages = Message.for_user(valid_token.user_id).page(params[:page]).per(30)
    render :json => messages, :status => 200
  end

  def unread_count_for_user
    unread_count = Message.where(:recipient_id => valid_token.user_id, :read => false).count
    render :json => {unread_count: unread_count}, :status => 200
  end

  def mark_all_read
    messages = Message.where(:recipient_id => valid_token.user_id, :read => false)
    messages.each do |message|
      message.read = true
      message.save
    end
    render :json => {message: 'Marked all as read'}, :status => 200
  end

  def create
    recipient = User.find_by_username(params[:username])
    if recipient
      message = Message.new(:sender_id => valid_token.user_id, :recipient_id => recipient.id,
                            :content => params[:content], :read => false)
      if message.save
        render :json => {message: 'Message created'}, :status => 201
      else
        render :json => {error: 'Unable to create message'}, :status => 400
      end
    else
      render :json => {error: 'Recipient not found'}, :status => 400
    end
  end


end
