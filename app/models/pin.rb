class Pin < ActiveRecord::Base
  validates :user_id, :presence => true
  validates :article_id, :presence => true

  validates_uniqueness_of :user_id, :scope => :article_id

  belongs_to :user
  belongs_to :article

  def self.areas_with_picture(user_id)
    ids = where(user_id: user_id).pluck(:article_id)
    pin_groups = Article.joins(:restaurant).where(id: ids).order('restaurants.city, restaurants.name').group_by { |article|
      article.restaurant.city
    }
    areas ||= []
    pin_groups.each do |area_name, articles|
      areas << {
          area: area_name,
          photo: articles.sample.cover_photo.url
      }
    end
    return areas
  end

  def self.for_user_within_area(user_id, area_name)
    ids = where(user_id: user_id).pluck(:article_id)
    articles = Article.joins(:restaurant).where("articles.id IN (?) and restaurants.city = ?",
                                                ids, area_name).order('articles.title')

    meals_50 ||= []
    meals_100 ||= []
    meals_200 ||= []
    meals_300 ||= []
    desserts ||= []
    snacks ||= []

    articles.each do |article|
      if article.food_type == 0
        if article.meal_price > 0 && article.meal_price <= 50
          meals_50 << article
        elsif article.meal_price > 50 && article.meal_price <= 100
          meals_100 << article
        elsif article.meal_price > 100 && article.meal_price <= 200
          meals_200 << article
        else
          meals_300 << article
        end
      elsif article.food_type == 1
        desserts << article
      else
        snacks << article
      end
    end

    combined_articles = meals_50 + meals_100 + meals_200 + meals_300 + desserts + snacks

    return combined_articles, meals_50.count, meals_100.count, meals_200.count, meals_300.count,
        desserts.count, snacks.count

  end

  def self.for_user(user_id)
    items_limit = Rails.application.config.items_limit
    where(:user_id => user_id).order('created_at DESC')
  end

  # def self.for_user_by_area(user_id)
  #   items_limit = Rails.application.config.items_limit
  #   where(:user_id => user_id).order('area, created_at').limit(items_limit)
  # end

  def self.for_user_by_area(user_id)
    pins = where(:user_id => user_id)
    ids ||= []
    pins.each do |pin|
      ids << pin.article_id
    end
    articles = Article.joins(:restaurant).where(id: ids).order('restaurants.city, restaurants.name').group_by {|article| article.restaurant.city }
    return articles #.to_json(:include => [:restaurant, :subcontents, :user])
  end

end
