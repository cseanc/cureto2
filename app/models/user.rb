class User < ActiveRecord::Base
  require 'securerandom'
  before_create :confirmation_token

  validates :username, :presence => true, :uniqueness => true
  validates :email, :presence => true, :uniqueness => true

  has_secure_password

  has_many :tokens, :dependent => :destroy
  has_many :articles, :dependent => :destroy

  has_many :pins, :dependent => :destroy
  has_many :stars, :dependent => :destroy

  mount_uploader :cover_photo, CoverPhotoUploader

  def image_data=(data)
    io = CarrierStringIO.new(Base64.decode64(data))
    self.cover_photo = io
  end

  def self.discover
    users = joins(:articles).group('users.id').where('articles.draft = ?', false)
  end

  def confirm_email
    self.email_confirmed = true
    self.confirm_token = nil
    save!(:validate => false)
  end

  def send_password_reset_token
    self.password_reset_token = SecureRandom.urlsafe_base64.to_s
    self.password_reset_sent_at = Time.now
    save!(:validate => false)
    UserMailer.password_reset(self).deliver_now
  end

  private
  def confirmation_token
    if self.confirm_token.blank?
      self.confirm_token = SecureRandom.urlsafe_base64.to_s
    end
  end

end
