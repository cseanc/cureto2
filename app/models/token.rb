class Token < ActiveRecord::Base
  before_create :create_token_string
  belongs_to :user

  private
  def create_token_string
    self.token_string = SecureRandom.uuid
  end

end
