class Article < ActiveRecord::Base
  validates :title, :presence => true
  validates :content, :presence => true
  validates :cover_photo_width, :presence => true
  validates :cover_photo_height, :presence => true
  validates :user_id, :presence => true
  validates :food_type, :presence => true
  
  belongs_to :user
  belongs_to :restaurant
  has_many :subcontents, -> {order 'sequence'}, :dependent => :destroy

  has_many :pins, :dependent => :destroy
  has_many :stars, :dependent => :destroy
  
  mount_uploader :cover_photo, ArticlePhotoUploader
  
  def image_data=(data)
    io = CarrierStringIO.new(Base64.decode64(data))
    self.cover_photo = io
  end
  
  def self.home
    not_draft_desc
  end

  def self.for_type_and_price(type=0, price=50)
    floated_price = price.to_f
    return where('draft = ? and food_type = ? and meal_price > ?', false, type, 200).order('created_at DESC') if floated_price > 200
    where('draft = ? and food_type = ? and meal_price <= ?', false, type, floated_price).order('created_at DESC')
  end

  def self.for_type(type=0)
    where(:draft => false, :food_type => type).order('created_at DESC')
  end

  def self.for_price(price=50)
    floated_price = price.to_f
    return where('draft = ? and meal_price > ?', false, 200).order('created_at DESC') if floated_price > 200
    where('draft = ? and meal_price <= ?', false, floated_price).order('created_at DESC')
  end
  
  def self.not_draft_desc
    where(:draft => false).order('created_at DESC')
  end
  
  def self.not_draft_desc_for_user(user_id)
    where(:user_id => user_id, :draft => false).order('created_at DESC')
  end
  
  def self.draft_desc_for_user(user_id)
    where(:user_id => user_id, :draft => true).order('created_at DESC')
  end

  def self.not_draft_desc_for_restaurant(restaurant_id)
    where(:restaurant_id => restaurant_id, :draft => false).order('created_at DESC')
  end

  def self.not_draft_curated
    where("draft = ? and curated = ? and created_at <= ?", false, true, Time.now - 1.month)
  end

  # def self.for_pins_by_area(user_id)
  #   pins = Pin.where(:user_id => user_id)
  #   ids ||= []
  #   pins.each do |pin|
  #     ids << pin.article_id
  #   end
  #   select('*').joins(:restaurant).where(id: ids).order('restaurants.city, articles.created_at DESC').group_by {|article| article.restaurant.city }.as_json
  # end

  # def self.unique_restaurants
  #   # joins(:restaurant).group('articles.restaurant_id')
  #   # group('restaurant_id').order('RAND()')
  # end

  def self.chill(area='Everywhere', type=0, lower_price=0, upper_price=50)
    limit = 12
    floated_lower = lower_price.to_f
    floated_upper = upper_price.to_f
    if area == 'Everywhere'
      restaurants = Article.where('draft = ? and discover = ? and food_type = ? and meal_price > ? and meal_price <= ?',
                                  false, true, type, floated_lower, floated_upper).order('RAND()').limit(limit)
    else
      restaurants = Article.joins(:restaurant).where('draft = ? and discover = ? and food_type = ? and meal_price > ? and meal_price <= ? and restaurants.city = ?',
                                  false, true, type, floated_lower, floated_upper, area).order('RAND()').limit(limit)
    end
    unique_restaurants ||= []
    restaurants.group_by(&:restaurant_id).each do |restaurant, articles|
      unique_restaurants << articles[rand(articles.size-1)]
    end
    return unique_restaurants
  end

  def self.explore(area)
    articles = Article.joins(:restaurant).where('draft = ? and discover = ? and restaurants.city = ?',
                          false, true, area).order('RAND()')

    ids ||= []
    unique_articles ||= []
    articles.each do |article|
      if !ids.include?(article.restaurant.id)
        ids << article.restaurant_id
        unique_articles << article
      end
    end

    meals_50 ||= []
    meals_100 ||= []
    meals_200 ||= []
    meals_300 ||= []
    desserts ||= []
    snacks ||= []

    unique_articles.each do |article|
      if article.food_type == 0
        if article.meal_price > 0 && article.meal_price <= 50
          meals_50 << article
        elsif article.meal_price > 50 && article.meal_price <= 100
          meals_100 << article
        elsif article.meal_price > 100 && article.meal_price <= 200
          meals_200 << article
        else
          meals_300 << article
        end
      elsif article.food_type == 1
        desserts << article
      else
        snacks << article
      end
    end

    combined_articles = meals_50 + meals_100 + meals_200 + meals_300 + desserts + snacks

    return combined_articles, meals_50.count, meals_100.count, meals_200.count, meals_300.count,
        desserts.count, snacks.count

  end
  
end
