class Message < ActiveRecord::Base
  validates :sender_id, :presence => true
  validates :recipient_id, :presence => true
  validates :content, :presence => true

  def self.for_user(user_id)
    where(:recipient_id => user_id).order('created_at DESC')
  end

end
