class FeaturedGroup < ActiveRecord::Base
  has_many :featured_elements, :dependent => :destroy
end
