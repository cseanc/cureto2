class Subcontent < ActiveRecord::Base
  validates :subtitle, :presence => true
  validates :sequence, :presence => true
  validates :subphoto_width, :presence => true
  validates :subphoto_height, :presence => true
  
  belongs_to :article
  
  mount_uploader :subphoto, ArticlePhotoUploader
  
  def image_data=(data)
    io = CarrierStringIO.new(Base64.decode64(data))
    self.subphoto = io
  end
  
end
