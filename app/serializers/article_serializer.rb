class ArticleSerializer < ActiveModel::Serializer
  attributes :id, :title, :content,
             :cover_photo, :cover_photo_width, :cover_photo_height,
             :food_type, :meal_type, :meal_price, :draft, :stars_count
  
  has_one :user
  has_one :restaurant
  has_many :subcontents
  
end
