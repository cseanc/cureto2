class MessageSerializer < ActiveModel::Serializer
  attributes :id, :sender_id, :recipient_id, :content, :read, :created_at
end
