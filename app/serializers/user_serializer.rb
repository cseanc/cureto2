class UserSerializer < ActiveModel::Serializer
  attributes :username, :display_name, :bio, :cover_photo, :public_articles_count
end
