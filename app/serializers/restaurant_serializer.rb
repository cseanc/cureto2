class RestaurantSerializer < ActiveModel::Serializer
  attributes :id, :identifier, :name, :street, :city, :country, :latitude, :longitude, :category_id, :category_name
end
