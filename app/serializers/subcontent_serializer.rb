class SubcontentSerializer < ActiveModel::Serializer
  attributes :id, :subtitle, :subphoto, :subphoto_width, :subphoto_height, :article_id
end
