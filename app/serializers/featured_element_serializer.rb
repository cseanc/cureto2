class FeaturedElementSerializer < ActiveModel::Serializer
  attributes :id, :target_id
end
