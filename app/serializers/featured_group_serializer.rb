class FeaturedGroupSerializer < ActiveModel::Serializer
  attributes :id, :featured_type, :title, :description, :cover_photo_url, :opacity
end
