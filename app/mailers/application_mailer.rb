class ApplicationMailer < ActionMailer::Base
  default from: ENV['CURETO_EMAIL_ADDRESS']
end
