class UserMailer < ApplicationMailer

  def registration_confirmation(user)
    @customer = user
    attachments.inline['appicon.png'] = File.read('app/assets/images/appicon.png')
    mail(:to => @customer.email, :subject => "Registration Confirmation")
  end

  def password_reset(user)
    @customer = user
    attachments.inline['appicon.png'] = File.read('app/assets/images/appicon.png')
    mail(:to => @customer.email, :subject => "Password Reset")
  end

end